(function() {

  //'use strict';

  angular
    .module('tokenAuthApp.components.main', ['ngRoute', 'tokenAuthApp.services'])
    .controller('mainController', mainController)
    .directive('clientCard', ['clientService','$window', '$timeout', function(clientService, $window, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'js/components/directives/client-card.html',
        scope: {
          client: '='
        },
        link: function(scope, element) {
          scope.isCollapsed = true;
          scope.isCollapsedProduct = true;
          scope.isCollapsedClientEdit = true;
          scope.addProduct = true;
          $timeout(() => {
            scope.isLoggedIn = scope.$parent.isLoggedIn;
          }, 1000);

          scope.addNewProduct = function() {

            let newProduct = {
              product_name: scope.newProductName,
              product_description: scope.newProductDescription
            };

            clientService.createProduct(scope.client._id, newProduct)
            .then((response) => {
              if (response.data.status === 'success') {
                $window.location.reload();
              }
            })
            .catch((err) => {
              console.log('Can\'t create new product. Error:\n' + err.status);
            });
          };

          scope.deleteClient = function() {
            clientService.deleteClient(scope.client._id)
            .then((response) => {
              if (response.data.status === 'success') {
                $window.location.reload();
              }
            })
            .catch((err) => {
              console.log('Could not delete client. Error: ' + err.status);
            });
          };

          scope.editClient = function() {
            let client = {};

            client.name = !scope.editClientName ? scope.client.name : scope.editClientName;
            client.description = !scope.editClientDescription ? scope.client.description : scope.editClientDescription;

            clientService.editClient(scope.client._id, client)
            .then((response) => {
              if (response.data.status === 'success') {
                $window.location.reload();
              }
            })
            .catch((err) => {
              console.log('Could not edit client. Error:\n' + err.status);
            });
          };
        }
      };
    }])
    .directive('productCard', ['clientService','$window', '$timeout', function(clientService, $window, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'js/components/directives/product-card.html',
        scope: {
          product: '='
        },
        link: function(scope, element) {
          scope.isCollapsed = true;
          scope.isCollapsedProductEdit = true;
          scope.clientId = scope.$parent.client._id;
          $timeout(() => {
            scope.isLoggedIn = scope.$parent.isLoggedIn;
          }, 1000);

          scope.editProduct = function() {
            let data = {};
            data.product_name = !scope.editProductName ? scope.product.product_name : scope.editProductName;
            data.product_description = !scope.editProductDescription ? scope.product.product_description : scope.editProductDescription;

            clientService.editProduct(scope.clientId, scope.product.product_name, data)
            .then((response) => {
              if (response.data.status === 'success') {
                $window.location.reload();
              }
            })
            .catch((err) => {
              console.log('Could not edit product. Error: ' + err.status + ' ' + err.message);
            });
          };

          scope.deleteProduct = function() {
            clientService.deleteProduct(scope.clientId, scope.product.product_name)
            .then((response) => {
              if (response.data.status === 'success') {
                $window.location.reload();
              }
            })
            .catch((err) => {
              console.log('Could not delete product. Error: ' + err.status + ' ' + err.message);
            });
          };
        }
      };
    }]);

  mainController.$inject = ['$scope', 'clientService', 'authService', '$location', '$window'];

  function mainController($scope, clientService, authService, $location, $window) {
    const vm = this;
    $scope.isLoggedIn = false;
    let token;

    function getToken() {
      token = localStorage.getItem('token');
    }

    getToken();

    function checkIfLoggedIn() {
      getToken();

      if (token) {
        authService.ensureAuthenticated(token)
        .then((user) => {
          if (user.data.status === 'success') {
            $scope.isLoggedIn = true;
          } else {
            $scope.isLoggedIn = false;
            localStorage.removeItem('token');
            $location.path('/login');
          }
        })
        .catch((err) => {
          console.log('There has been an error with authentication. Error:\n' + err);
        });
      }
    }

    checkIfLoggedIn();

    $scope.checkStatus = checkIfLoggedIn;

    $scope.anyClients = false;

    $scope.newClient = {};

    clientService.getAllClients()
    .then((response) => {
      if (response.data.length > 0) {
        $scope.clients = response.data.reverse();
        $scope.anyClients = true;
      }
    })
    .catch((err) => {
      console.log('There seems to be a problem with getting the clients. Errors are:\n' + err);
    });

    $scope.createClient = function() {
      clientService.createClient($scope.newClient, token)
      .then((response) => {
        if (response.data.status === 'success') {
          console.log('Successfuly created a new client');
          $window.location.reload();
        }
      })
      .catch((err) => {
        console.log('Can\'t create user, there seems to be a problem. Errors:\n' + err);
      });
    };
  }

})();
