(function() {

  'use strict';

  angular
    .module('tokenAuthApp.services', [])
    .service('authService', authService)
    .service('clientService', clientService);

  authService.$inject = ['$http'];
  clientService.$inject = ['$http'];

  /*jshint validthis: true */
  function clientService($http) {
    const baseURL = 'http://limitless-hamlet-17725.herokuapp.com/';

    this.getAllClients = function() {
      return $http({
        method: 'GET',
        url: baseURL
      });
    };

    this.createClient = function(client, token) {
      return $http({
        method: 'POST',
        url: baseURL + 'auth/client/',
        data: { name: client.name, description: client.description },
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        }
      });
    };

    this.createProduct = function(client_id, data) {
      const token = localStorage.getItem('token');
      return $http({
        method: 'POST',
        url: baseURL + 'auth/client/' + client_id + '/product',
        data: data,
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        }
      });
    };

    this.editClient = function(client_id, data) {
      const token = localStorage.getItem('token');
      return $http({
        method: 'PATCH',
        url: baseURL + 'auth/client/' + client_id,
        data: data,
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        }
      });
    };

    this.deleteClient = function(client_id) {
      const token = localStorage.getItem('token');
      return $http({
        method: 'DELETE',
        url: baseURL + 'auth/client/' + client_id,
        headers: {
          Authorization: 'Bearer ' + token
        }
      });
    };

    this.editProduct = function(clientId, product_name, data) {
      const token = localStorage.getItem('token');
      return $http({
        method: 'PATCH',
        url: baseURL + 'auth/client/' + clientId + '/product/' + product_name,
        data: data,
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        }
      });
    };

    this.deleteProduct = function(clientId, product_name) {
      const token = localStorage.getItem('token');
      return $http({
        method: 'DELETE',
        url: baseURL + 'auth/client/' + clientId + '/product/' + product_name,
        headers: {
          Authorization: 'Bearer ' + token
        }
      });
    };

  }

  function authService($http) {
    const baseURL = 'http://limitless-hamlet-17725.herokuapp.com/auth/';
    this.login = function(user) {
      return $http({
        method: 'POST',
        url: baseURL + 'login',
        data: user,
        headers: {'Content-Type': 'application/json'}
      });
    };
    this.register = function(user) {
      return $http({
        method: 'POST',
        url: baseURL + 'register',
        data: user,
        headers: {'Content-Type': 'application/json'}
      });
    };
    this.ensureAuthenticated = function(token) {
      return $http({
        method: 'GET',
        url: baseURL + 'user',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token
        }
      });
    };
  }
}
)();
